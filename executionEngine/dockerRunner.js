'use strict';

const { Docker } = require('node-docker-api');
const docker = new Docker({ socketPath: '/var/run/docker.sock' });

const DockerRunner = function(imageName, timeout) {
    this.imageName = imageName;
    this.timeout = timeout;

    this.stopContainer = (container, timeout) => {
         setTimeout(()=> {
             container.stop();
             console.log(`Stopping ${container.id} after ${timeout}`);
         }, timeout)
     };

    this.runLanguageEngine = (bucketName, key) => {
        docker.container.create({
          Image: this.imageName,
          Env: [
              `BUCKET_NAME=${bucketName}`,
              `KEY=${key}`,
          ],
        })
          .then(container => container.start())
          .then(container => this.stopContainer(container, this.timeout))
          .catch(error => console.log(error));
    };
};

module.exports = DockerRunner;