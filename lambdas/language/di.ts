import Service from '../lib/Service';
import LambdaCompileRunService from '../lib/LambdaCompileRunService';
import CandidateCodeS3 from '../lib/CandidateCodeS3';
import {ScriptWithNoOutput} from '../lib/Script';
import * as child_process from 'child_process';
import {JavaScript, CSharpScript} from '../lib/Script';


const scriptWithNoOutput = new ScriptWithNoOutput(child_process.exec);

export const createJavaService = (candidateCodeS3: CandidateCodeS3): Service => {
    const javaRunScript = new JavaScript(child_process.exec);
    return new LambdaCompileRunService(candidateCodeS3, javaRunScript, scriptWithNoOutput);
};

export const createCsharpService = (candidateCodeS3: CandidateCodeS3): Service => {
  const cSharpRunScript = new CSharpScript(child_process.exec);
  return new LambdaCompileRunService(candidateCodeS3, cSharpRunScript, scriptWithNoOutput);
};
