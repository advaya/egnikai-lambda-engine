 #!/bin/bash
set -e
if [ "$#" -ne 2 ]; then
    echo "usage sh createTgzAndUpload.sh environment <aws cli profile name>"
    exit 1
fi

environment=$1
profile=$2

cd jdk1.8
tar zcvf java-1.8.0-openjdk-devel.tgz *
aws s3 cp ./java-1.8.0-openjdk-devel.tgz s3://tw-egnikai-${environment}-runtimes/java-1.8.0-openjdk-devel.tgz \
    --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers \
    --profile ${profile}
rm -rf java-1.8.0-openjdk-devel.tgz
cd ..