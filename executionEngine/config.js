const config = {
    imageName: process.env.IMAGE_NAME || "puneethp/runner:csharp",
    timeOut: process.env.TIME_OUT || "30000"
};

module.exports = config;