#!/bin/bash
set -e
environment=$1
DIRECTORY_OF_SCRIPT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

export JAVA_HOME="/tmp/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.77-0.b03.9.amzn1.x86_64"

curl -sSL https://s3.amazonaws.com/tw-egnikai-${environment}-runtimes/java-1.8.0-openjdk-devel.tgz | tar -xz -C /tmp > /dev/null

# For some reason, libjvm.so needs to be physically present
# Can't symlink it, have to copy, but everything else can be symlinks
cp -as /usr/lib/jvm/java-1.8*/jre /tmp/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.77-0.b03.9.amzn1.x86_64/
rm /tmp/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.77-0.b03.9.amzn1.x86_64/jre/lib/amd64/server/libjvm.so
cp /usr/lib/jvm/java-1.8*/jre/lib/amd64/server/libjvm.so /tmp/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.77-0.b03.9.amzn1.x86_64/jre/lib/amd64/server/
export PATH=/tmp/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.77-0.b03.9.amzn1.x86_64/bin:$PATH

mkdir -p /tmp/downloadedCode
cd /tmp
tar -xzf Candidate.tgz -C ./downloadedCode > /dev/null

mkdir -p /tmp/downloadedCode/classes
javac -nowarn -d downloadedCode/classes $(find /tmp/downloadedCode/src/main/java/ -name "*.java")
javac -nowarn -d downloadedCode/classes -cp "/tmp/downloadedCode/classes/:/tmp/junitlibs/*" $(find /tmp/downloadedCode/src/test/java -name "*.java")

testFilenames=$(node ${DIRECTORY_OF_SCRIPT}/findTestClassNames.js /tmp/downloadedCode/classes)
java -cp "/tmp/downloadedCode/classes/:/tmp/junitlibs/*" org.junit.runner.JUnitCore ${testFilenames}

