import Response, {ErrorType} from '../Response/Response';

class JavaErrorParser {

  private static TEST_FAILED_REGEX = /\d+\) (.*)/;

  static parse(stdOut: string): Response {
    let lines = stdOut.split('\n');
    lines = JavaErrorParser.ignoreEmptyLineAndWarnings(lines);

    if (lines[0].includes('JUnit version 4.12')) {
      return JavaErrorParser.parseAndCreateTestFailureOutput(lines);
    } else {
      return JavaErrorParser.createCompilationFailureOutput(stdOut);
    }
  }

  private static parseAndCreateTestFailureOutput(lines: string[]): Response {
    const failedTests = [];
    // const failedNormalTests: string[] = [];
    // const failedHiddenTests: string[] = [];
    for (const line of lines) {
      const match = JavaErrorParser.TEST_FAILED_REGEX.exec(line);
      if (match && match.length >= 2){
        var testCaseNames = match[1].split("(");
        failedTests.push(testCaseNames[0]);

        // if(match[1].toLowerCase().includes('hidden')) {
        //   failedHiddenTests.push(match[1]);
        // }
        // else {
        //   failedNormalTests.push(match[1]);
        // }
      }
    }

    let message = 'There were test failures.';
    // if(failedHiddenTests.length > 0){
    //   message = message.concat('\n');
    //   message = message.concat(failedHiddenTests.reduce((acc, testName) =>  acc + "\n" + testName));
    // }
    // if(failedNormalTests.length > 0){
    //   message = message.concat('\n');
    //   message = message.concat(failedNormalTests.reduce((acc, testName) =>  acc + "\n" + testName));
    // }
     if(failedTests.length > 0){
      message = message.concat('\n');
      message = message.concat(failedTests.reduce((acc, testName) =>  acc + "\n" + testName));
    }
    return {success: false, errorType: ErrorType.TESTS_FAILED, message};
  }

  private static createCompilationFailureOutput(stdout: string): Response {
      let filePath = stdout.split(':')[0];
      let fName = filePath.substring(filePath.lastIndexOf('/'))
    return {success: false, errorType: ErrorType.COMPILATION_FAILED, message: stdout.replace(filePath,fName)};
  }

  private static ignoreEmptyLineAndWarnings(lines: string[]): string[] {
    return lines.filter( line => line && !line.includes("Note"));
  }
}

export default JavaErrorParser;