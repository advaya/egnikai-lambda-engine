var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../app');

var expect = chai.expect;

chai.use(chaiHttp);

describe('App', () => {
  describe('/test', () => {
    it('should expose health check endpoint', (done) => {
      chai.request(app)
        .get('/test')
        .end((error, response) => {
          expect(error).equal(null);
          expect(response.statusCode).equal(200);
          expect(response.text).equal('Server is running');
          done();
        });
    });
  });

  describe('/execute', () => {
    it('should respond with bad request when bucket name and key is not passed', (done) => {
      chai.request(app)
        .post("/execute")
        .end((error, response) => {
          expect(error).equal(null);
          expect(response.statusCode).equal(400);
          expect(response.text).equal('Bucket Name and Key can not be empty');
          done();
        });
    });

    // it.skip('should expose execute endpoint', (done) => {
    //   const data = {
    //     bucketName: "testBucket",
    //     key: "test.tgz"
    //   };
    //   chai.request(app)
    //   .post('/execute')
    //   .send(data)
    //   .end((error, response) => {
    //     expect(error).equal(null);
    //     expect(response.statusCode).equal(200);
    //     expect(response.text).equal('OK');
    //     done();
    //   });
    // });
  });
});