import CandidateCodeS3 from './CandidateCodeS3';
import {Script, ScriptWithNoOutput} from './Script';
import Response from './Response/Response';
import Service from './Service';

class LambdaCompileRunService implements Service {
  constructor(private candidateCodeS3: CandidateCodeS3,
              private script: Script<any, any>,
              private scriptWithNoOutput: ScriptWithNoOutput) {}

  async execute(): Promise<void> {
    try {
      await this.scriptWithNoOutput.execute('rm -rf /tmp/*');
      console.time('downloadS3Code');
      await this.candidateCodeS3.download();
      console.timeEnd('downloadS3Code');
      console.time('execute code');
      let response = await this.executeScript();
      console.timeEnd('execute code');
      await this.candidateCodeS3.upload(response);
    } finally {
      console.time('delete temp');
      await this.scriptWithNoOutput.execute('rm -rf /tmp/*');
      console.timeEnd('delete temp');
    }
  }

  private async executeScript() {
    let response: Response;
    try {
      response = await this.script.execute();
    } catch (errorResponse) {
      response = errorResponse;
    }
    return response;
  }
}

export default LambdaCompileRunService;