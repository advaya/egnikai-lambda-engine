import Response, {ErrorType} from './Response';

describe('Response Tests', () => {
  it('should return the right json for tests failed', () => {
    const response: Response = {success: false, errorType: ErrorType.TESTS_FAILED, message: 'test failed'};

    expect(JSON.stringify(response)).toEqual('{\"success\":false,\"errorType\":\"TESTS_FAILED\",\"message\":\"test failed\"}');
  });
});