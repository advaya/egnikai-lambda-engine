interface Response {
  success: boolean;
  errorType?: ErrorType;
  message?: string;
}

export enum ErrorType {
  COMPILATION_FAILED = 'COMPILATION_FAILED',
  TESTS_FAILED = 'TESTS_FAILED',
}

export default Response;