import 'source-map-support/register';
import dalamb from 'dalamb';
import {S3CreateEvent} from 'aws-lambda';
import {createJavaService} from '../language/di';
import CandidateCodeS3 from '../lib/CandidateCodeS3';
import * as fs from 'fs';
import AWS = require('aws-sdk');

export default dalamb<S3CreateEvent>(async event => {
  const bucket = event.Records[0].s3.bucket.name;
  const key = event.Records[0].s3.object.key;

  const s3 = new AWS.S3();
  const candidateCode = new CandidateCodeS3(bucket, key, s3, fs.createWriteStream)
  console.log(`running java compile and run for ${bucket}/${key}`);
  await createJavaService(candidateCode).execute();
  console.log(`completed running java compile and run for ${bucket}/${key}`);
});
