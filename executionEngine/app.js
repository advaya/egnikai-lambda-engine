'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const DockerRunner  = require('./dockerRunner');

var config = require('./config');
const dockerRunner = new DockerRunner(config.imageName, config.timeOut);

const app = express();

app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({
  extended: true
}));

app.get('/test', (req, res) => res.send('Server is running'));

app.post("/execute", (req, res) => {
    const bucketName = req.body.bucketName;
    const key = req.body.key;
    if(!bucketName || !key){
        return res.status(400).send("Bucket Name and Key can not be empty");
    }

    dockerRunner.runLanguageEngine(bucketName, key);
    res.sendStatus(200);
});

app.listen(3000, () => console.log('Example app listening on port 3000!'));

module.exports = app;