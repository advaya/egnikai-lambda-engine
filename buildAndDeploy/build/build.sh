#!/usr/bin/env bash
set -e

if [ "$#" -ne 2 ]; then
    echo "usage sh build.sh <aws cli profile name> <aws region name>"
    exit 1
fi

profileName=$1
region=$2

cfn-create-or-update \
  --stack-name ecr-language-runner-repo \
  --template-body file://buildAndDeploy/build/ecr-runner.yml \
  --region ${region} \
  --profile ${profileName} \
  --wait

cfn-create-or-update \
 --stack-name egnikai-execution-engine \
 --template-body file://buildAndDeploy/build/execution-engine-bucket.yml \
 --capabilities CAPABILITY_NAMED_IAM \
 --region us-east-1 \
 --profile default \


commitHash=$(git rev-parse --short HEAD)

cp -a binaries/CSharp/. lambdas/

pushd lambdas
yarn install
yarn test
docker build -t egnikai-language-runner:${commitHash} .
rm -rf dotnet
popd

pushd executionEngine
npm install
npm test
artifact=${commitHash}.tar.tgz
COPYFILE_DISABLE=1 tar pzcf ${artifact} .
aws s3 cp ${artifact} s3://egnikai-execution-engine/${artifact} --profile ${profileName}
rm -rf ${artifact}

popd
