#!/bin/bash
set -e

export HOME=/dotnet
export PATH=/dotnet:$PATH

mkdir -p /tmp/downloadedCode
cd /tmp

tar -xzf Candidate.tgz -C ./downloadedCode > /dev/null

cd downloadedCode

dotnet new classlib -o src --no-restore
dotnet new mstest -o test --no-restore
dotnet add ./test/test.csproj package Microsoft.NET.Test.Sdk --no-restore
dotnet add ./test/test.csproj reference ./src/src.csproj
rm ./src/Class1.cs
rm ./test/UnitTest1.cs
cd test
dotnet test