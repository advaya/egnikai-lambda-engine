import Response, {ErrorType} from '../Response/Response';

class CsharpErrorParser {

  static parse(stdOut: string): Response {
    let output = stdOut.substring(stdOut.indexOf("Build started"));
    output = output.substring(output.indexOf("\n") + 1);

    if(output.includes("Test Run Failed")){
      let lines = output.split('\n');
      lines = CsharpErrorParser.ignoreEmptyLines(lines);
      return CsharpErrorParser.parseAndCreateTestFailureOutput(lines);
    } else {
      return CsharpErrorParser.createCompilationFailureOutput(output);
    }
  }

  private static parseAndCreateTestFailureOutput(lines: string[]): Response {
    const failedTestNames = lines.filter(line => line.startsWith("Failed"))
      .reduce((acc, line) => acc + "\n" + line);
    
    let message = 'There were test failures.\n';
    message = message.concat(failedTestNames);
    return {success: false, errorType: ErrorType.TESTS_FAILED, message};
  }

  private static createCompilationFailureOutput(stdout: string): Response {
    return {success: false, errorType: ErrorType.COMPILATION_FAILED, message: stdout};
  }

  private static ignoreEmptyLines(lines: string[]): string[] {
    return lines.filter( line => line);
  }
}

export default CsharpErrorParser;