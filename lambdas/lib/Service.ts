interface Service {
  execute(): Promise<void>;
}

export default Service;