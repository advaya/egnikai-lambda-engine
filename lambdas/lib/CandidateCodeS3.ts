import {S3, AWSError, } from 'aws-sdk';
import {PathLike, WriteStream} from 'fs';
import Response from './Response/Response';
import {PromiseResult} from 'aws-sdk/lib/request';
import {PutObjectOutput} from 'aws-sdk/clients/s3';

const DOWNLOADED_FILE_DESTINATION = '/tmp/Candidate.tgz';

class CandidateCodeS3 {
  constructor(private bucket: string,
              private key: string,
              private s3: S3,
              private createWriteStream: ((path: PathLike) => WriteStream)) {
  }

  getQualifiedKeyName(): string {
    return `s3://${this.bucket}/${this.key}`;
  }

  download(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      console.log("starting to download");
      const writeFileStream = this.createWriteStream(DOWNLOADED_FILE_DESTINATION);
      this.s3.getObject({Bucket: this.bucket, Key: this.key})
        .createReadStream()
        .on('end', () => {
            console.log("download completed");
            return resolve();
        })
        .on('error', (error) => {
            console.log("download error", error);
          return reject(error);
        })
        .pipe(writeFileStream);
    });
  }

  upload(response: Response): Promise<PromiseResult<PutObjectOutput, AWSError>> {
    const params = {
      Bucket: this.bucket,
      Key: this.getKeyForResponseJson(),
      ContentType: 'application/json',
      Body: Buffer.from(JSON.stringify(response)),
    };
    return this.s3.putObject(params).promise();
  }

  private getKeyForResponseJson() {
    const filePathWithoutExtension = this.key.substring(0, this.key.lastIndexOf('.'));
    return `${filePathWithoutExtension}.json`;
  }
}

export default CandidateCodeS3;