import 'source-map-support/register';
import dalamb from 'dalamb';
import {S3CreateEvent} from 'aws-lambda';
import EC2CompileRunService from '../lib/EC2CompileRunService'

export default dalamb<S3CreateEvent>(async event => {
  const bucket = event.Records[0].s3.bucket.name;
  const key = event.Records[0].s3.object.key;

  console.log(`running csharp compile and run for ${bucket}/${key}`);
  await new EC2CompileRunService(bucket, key).execute();
  console.log(`completed running csharp compile and run for ${bucket}/${key}`);
});
