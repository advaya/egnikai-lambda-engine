import Service from './LambdaCompileRunService';
import CandidateCodeS3 from './CandidateCodeS3';
import {JavaScript, ScriptWithNoOutput} from './Script';
import {ErrorType} from './Response/Response';

describe('ServiceTests', () => {
  let mockCandidateCodeS3: CandidateCodeS3;
  let javaGradleScript: JavaScript;
  let scriptWithNoOutput: ScriptWithNoOutput;

  beforeEach(() => {
    mockCandidateCodeS3 = jest.genMockFromModule('./CandidateCodeS3');
    mockCandidateCodeS3.download = jest.fn().mockResolvedValue(undefined);
    mockCandidateCodeS3.upload = jest.fn().mockResolvedValue({});

    javaGradleScript = jest.genMockFromModule('./Script');
    javaGradleScript.execute = jest.fn().mockResolvedValue({success: true});

    scriptWithNoOutput = jest.genMockFromModule('./Script');
    scriptWithNoOutput.execute = jest.fn().mockResolvedValue({});
  });

  it('should download Candidates code from S3', async () => {
    const service = new Service(mockCandidateCodeS3, javaGradleScript, scriptWithNoOutput);
    await service.execute();

    // @ts-ignore
    expect(mockCandidateCodeS3.download.mock.calls.length).toBe(1);
  });

  it('should execute the script initCompileAndRun.sh ', async () => {
    const service = new Service(mockCandidateCodeS3, javaGradleScript, scriptWithNoOutput);
    await service.execute();

    // @ts-ignore
    expect(javaGradleScript.execute.mock.calls.length).toBe(1);
  });

  it('should execute the script and pass the response to be uploaded to S3', async () => {
    const service = new Service(mockCandidateCodeS3, javaGradleScript, scriptWithNoOutput);
    await service.execute();

    // @ts-ignore
    expect(mockCandidateCodeS3.upload.mock.calls.length).toBe(1);
    // @ts-ignore
    expect(mockCandidateCodeS3.upload.mock.calls[0][0]).toEqual({success: true});
  });

  it('should upload error response from java scrip to S3', async () => {
    const errorResponse = {success: false, errorType: ErrorType.TESTS_FAILED, message: ''};
    javaGradleScript.execute = jest.fn().mockRejectedValue(errorResponse);

    const service = new Service(mockCandidateCodeS3, javaGradleScript, scriptWithNoOutput);
    await service.execute();

    // @ts-ignore
    expect(mockCandidateCodeS3.upload.mock.calls.length).toBe(1);
    // @ts-ignore
    expect(mockCandidateCodeS3.upload.mock.calls[0][0]).toBe(errorResponse);
  });

  it('should delete the tmp directory at the start and end of the execution', async () => {
    const service = new Service(mockCandidateCodeS3, javaGradleScript, scriptWithNoOutput);
    await service.execute();

    // @ts-ignore
    expect(scriptWithNoOutput.execute.mock.calls.length).toBe(2);
    // @ts-ignore
    expect(scriptWithNoOutput.execute.mock.calls[0][0]).toBe('rm -rf /tmp/*');
    // @ts-ignore
    expect(scriptWithNoOutput.execute.mock.calls[1][0]).toBe('rm -rf /tmp/*');
  });

});