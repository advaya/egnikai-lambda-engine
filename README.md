# egnikai-lambda-engine
Project consists of 2 modules.
- __Lamda__ (For all Languages)
- __Execution Engine__ (For `C#`)

## 1. Lambda Module:
 - Consists of handler functions to define aws lambdas for execution of candidate code in sandboxed environment.
 - Each language has its specific lambda function defined for it.
 - All the function entry point has been defined in `serverless.yml`.
 - Only for `C#` we have created a special entry point `language/task.ts` which takes care of compile and run `C#` code and we create a docker image out of this.

## 2. Execution Engine (Special Case)
 - Since lambda was not able to run `C#` code we had to go for alternatives.
 - We have defined `executionEngine` node server hosted in some ec2 instance exposing certain port which takes care of spinning up docker-container with the image defined by `lambda module` to execute candidate code. 
 - `C#-Lambda` now just hits this endpoint with candidate code information.

### Understanding resposibility of Lambda:
- Lambda defined for each language gets triggered when candidate code is uploaded to respective bucket in `S3`.
- Each lambda downloads respective `SDK binary.tar.tgz` and use it to compile and run tests against candidate code.
- Lambda will take care of uploading result of execution 
-- _Compilelation Error_
-- _Test failure_
-- or any other _error_
as `json` format back to the same `S3` bucket

## About Pipeline:
- We are using `GO-CD` as our pipeline for CI and CD.
- The pipeline is configured through `pipeline.gocd.yml` that you can see in each repository.
- This uses `buildAndDeploy` folder to execute task for each commit in the repo.

### Stages
#### Build Stage
- It runs all the tests and builds the docker image of `languge runner` and artifact of `executionEngine` module.
- This stage is resposible for creating `aws ecr regestry` for docker `languge runner` image.
- It also creates `S3` bucket for publishing `executionEngine` artifacts.
- Finally it publishes the artifacts of `executionEngine` to already created `S3` bucket with name `${commitHash}.tar.tgz`
#### Deploy Stage
- This has 3 stages to deploy to `dev`, `qa` and `prod` environments.
- It uses the docker image already built from `Build` stage and retags `languge runner` image with `${env}-${commitHash}` and publish the image to `aws ecr regestry`
- Then it deploys each language lambdas to respective environments.
- Finally it also deploys cloudFormation stack to create `C#` executor with frunt facing  `LoadBalancer`

# Local Setup:

### Lambda Module
- It is __hard__ to do lambda testing in local. But you can always wish to deploy `test-lambda` with custom `trigger` and do your development.
- All you need to do is modify `serverless.yml` to deploy your `test-lambda` and run following command.
```
serverless deploy --key1 val1 --key2 val2 ...
```
### Execution Engine Module
- Its resposibility is to spin up `C#` execution container and run candidate code.
- Inputs to this is `Image Name` and `Time Out` which it expects to be set as env variables `IMAGE_NAME` and `TIME_OUT` respectively.
- __Make sure the `Image` is present in your local docker engine__
- Use the following command to run `executionEngine` locally which also _restarts_ the express server on and when files are modified.
```
npm run watch
```

# Maintaining Binaries
- `binaries` folder consists of all the supported language sdk with command to upload to `S3` bucket.
- For now the process is mannual to deploy these binaries to `dev`, `qa` and `prod` environment buckets.
- How to upload to `S3`?
```
cd <languageFolder>
sh createTgzAndUpload.sh param1 param2 ...
```