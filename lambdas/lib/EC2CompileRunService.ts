import axios from 'axios';
import Service from './Service';

class EC2CompileRunService implements Service {
  bucketName: string;
  key: string;

  constructor(bucketName: string, key: string) {
    this.bucketName = bucketName;
    this.key = key;
  }

  async execute(): Promise<void> {
    try {
      const endPoint:string = "http://" + process.env.LANGUAGE_RUNNER_ENDPOINT || "";
      if(!endPoint){
        console.log("Error: No endpoint configured for Language Runner");
        return Promise.reject(new Error("No endpoint configured for Language Runner"));
      }
      const responseData = await axios.post(endPoint.concat('/execute'), {
        bucketName: this.bucketName,
        key: this.key
      });
      console.log(responseData);
    } finally {
      console.log(`Compile for ${this.bucketName}/${this.key} started`);
    }
  }
}

export default EC2CompileRunService;