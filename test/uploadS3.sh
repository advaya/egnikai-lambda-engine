 #!/bin/bash
if [ -z "$1" ]; then
    echo "usage sh uplaodS3.sh <file.tgz> <aws cli profile name>"
    exit 1
fi
if [ -z "$2" ]; then
    echo "usage sh uploadS3.sh <file.tgz> <aws cli profile name>"
    echo "read this for more info https://docs.aws.amazon.com/cli/latest/userguide/cli-multiple-profiles.html"
    exit 1
fi

#aws s3 rm s3://tw-egnikai-dev-candidate-code-submissions/java --recursive --profile egnikai
aws s3 cp $1 s3://tw-egnikai-dev-candidate-code-submissions/runs/Java_8/999/1.tgz --profile $2