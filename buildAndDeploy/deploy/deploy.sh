#!/usr/bin/env bash

if [ "$#" -ne 7 ]; then
    echo "usage sh deploy.sh <aws cli profile name> <environment> <aws default region> 
        <instance type> <average CPU threshold> <ASG min size> <ASG max size>"
    exit 1
fi

profileName=$1
environment=$2
region=$3
instanceType=$4
averageCPUThreshold=$5
asgMinSize=$6
asgMaxSize=$7

commitHashOfDeployable=$(git rev-parse --short HEAD)

function retagAndPushBuiltImage() {
    accountId=$(aws sts get-caller-identity --query "Account" --profile ${profileName} --output text)
    commitHash=$(git rev-parse --short HEAD)
    $(aws ecr get-login --no-include-email --region ${region} --profile ${profileName})
    docker tag egnikai-language-runner:${commitHash} \
        ${accountId}.dkr.ecr.${region}.amazonaws.com/egnikai-language-runner:${environment}-${commitHash}
    docker push ${accountId}.dkr.ecr.${region}.amazonaws.com/egnikai-language-runner:${environment}-${commitHash}
}

function destroyCsharpRunnerStack(){
    cfn-create-or-update \
        --stack-name csharp-runner-${environment} \
        --template-body file://buildAndDeploy/deploy/csharp-launch-configuration.yml \
        --parameters ParameterKey=SecurityKeyName,ParameterValue=EgnikaiTeamKeypair \
                    ParameterKey=CommitHashOfDeployable,ParameterValue=${commitHashOfDeployable} \
                    ParameterKey=Environment,ParameterValue=${environment} \
                    ParameterKey=InstanceType,ParameterValue=${instanceType} \
                    ParameterKey=AvgCPUThreshold,ParameterValue=${averageCPUThreshold} \
                    ParameterKey=minSize,ParameterValue=0 \
                    ParameterKey=maxSize,ParameterValue=0 \
        --capabilities CAPABILITY_NAMED_IAM \
        --region ${region} \
        --profile ${profileName} \
        --wait || exit 1
}

function createCsharpRunnerStack(){
    cfn-create-or-update \
        --stack-name csharp-runner-${environment} \
        --template-body file://buildAndDeploy/deploy/csharp-launch-configuration.yml \
        --parameters ParameterKey=SecurityKeyName,ParameterValue=EgnikaiTeamKeypair \
                    ParameterKey=CommitHashOfDeployable,ParameterValue=${commitHashOfDeployable} \
                    ParameterKey=Environment,ParameterValue=${environment} \
                    ParameterKey=InstanceType,ParameterValue=${instanceType} \
                    ParameterKey=AvgCPUThreshold,ParameterValue=${averageCPUThreshold} \
                    ParameterKey=minSize,ParameterValue=${asgMinSize} \
                    ParameterKey=maxSize,ParameterValue=${asgMaxSize} \
        --capabilities CAPABILITY_NAMED_IAM \
        --region ${region} \
        --profile ${profileName} \
        --wait || exit 1
}

function createCsharpLoadBalancerLogsS3Bucket(){
    cfn-create-or-update \
        --stack-name csharp-load-balancer-logs-s3Bucket-${environment} \
        --template-body file://buildAndDeploy/deploy/csharp-load-balancer-logs-s3Bucket.yml \
        --parameters ParameterKey=Environment,ParameterValue=${environment} \
        --region ${region} \
        --profile ${profileName} \
        --wait || exit 1
}

function createEgnikaiRuntimes(){
    cfn-create-or-update \
        --stack-name tw-egnikai-${environment}-runtimes-s3Bucket \
        --template-body file://buildAndDeploy/deploy/execution-runtime-bucket.yml \
        --parameters ParameterKey=Environment,ParameterValue=${environment} \
        --region ${region} \
        --profile ${profileName} \
        --wait || exit 1
}

retagAndPushBuiltImage
createCsharpLoadBalancerLogsS3Bucket
destroyCsharpRunnerStack
createCsharpRunnerStack
createEgnikaiRuntimes

sh binaries/java/createTgzAndUpload.sh $environment default

languageRunnerEndpoint=$(aws elb describe-load-balancers --region us-east-1 | jq -r '.LoadBalancerDescriptions[] | select (.DNSName | contains ("'$environment\"')) | .DNSName')

echo "Language runner end point:" $languageRunnerEndpoint

cd lambdas
serverless deploy --environment ${environment} --languageRunnerEndpoint $languageRunnerEndpoint