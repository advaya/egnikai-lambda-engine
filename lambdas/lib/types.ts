import {ExecException} from 'child_process';

export type ExecFunction = (command: String,
                            callback: ((err: ExecException | null,
                                        stdOut: String,
                                        stdError: String) => void)) => void;
