import {compilationFailureConsoleOutput, testFailedConsoleOuput} from './testHelpers/TestConstants';
import CsharpErrorParser from './CsharpErrorParser';
import {ErrorType} from '../Response/Response';

describe('CSharpErrorParserTests', () => {

  it('should return the response with error type as compilation failed', () => {
    const response = CsharpErrorParser.parse(compilationFailureConsoleOutput);
    const expectedFailureMessage = "salaryIncrement/SalaryIncrementManagerTest.cs(23,13): error CS0246: The type or namespace name 'SalaryIncrementManager' could not be found (are you missing a using directive or an assembly reference?) [/tmp/downloadedCode/test/test.csproj]"
    expect(response).toEqual({success: false, errorType: ErrorType.COMPILATION_FAILED, message: expectedFailureMessage});
  });

  it('should return the response with error type as tests failed', () => {
    const response = CsharpErrorParser.parse(testFailedConsoleOuput);
    const testsFailureMessage = "There were test failures.\nFailed   ShouldReturnEmptyMapIfStringIsEmpty\nFailed   ShouldReturnCorrectCountIfStringContainsOneCharacter\nFailed   ShouldReturnCorrectCountIfStringContainsOneWord\nFailed   ShouldReturnCorrectCountIfStringContainsOneSentence\nFailed   ShouldReturnEmptyMapIfStringContainsOnlySpaces";
    expect(response).toEqual({success: false, errorType: ErrorType.TESTS_FAILED, message: testsFailureMessage});
  });

});