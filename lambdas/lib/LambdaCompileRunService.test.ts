import 'jest';
import {JavaScript, ScriptWithNoOutput} from './Script';
import {ExecException} from 'child_process';
import {JAVA_INIT_COMPILE_RUN} from './ScriptLocations';

describe('Initial Setup Tests', () => {
  describe('ScriptWithNoOutput Tests', () => {
    it('should invoke the script script correctly', async () => {
      const exec = jest.fn((_: String,
                            callback: (err: ExecException | null, stdOut: String, stdError: String) => void) => {
        callback(null, '', '');
      });

      const script = new ScriptWithNoOutput(exec);
      await script.execute('ls');

      expect(exec.mock.calls[0][0]).toEqual('ls');
    });

    it('should reject with the right error object if invoking the initCompileAndRun script threw an error', () => {
      const error = {code: 1, name: 'error', message: 'error occurred'};
      const exec = jest.fn((_: String,
                            callback: (err: ExecException | null, stdOut: String, stdError: String) => void) => {
        callback(error, '', 'Error occurred');
      });

      const script = new ScriptWithNoOutput(exec);
      return expect(script.execute('ls')).rejects.toBe(error);
    });

    it('should return a Promise at the end of a successful invocation', () => {
      const exec = jest.fn((_: String,
                            callback: (err: ExecException | null, stdOut: String, stdError: String) => void) => {
        callback(null, '', '');
      });

      const script = new ScriptWithNoOutput(exec);
      return expect(script.execute('ls')).resolves.toBe(undefined);
    });
  });

  describe('JavaGradleScriptTests', () => {
    it('should invoke the script initCompileAndRun correctly and return a response', async () => {
      const exec = jest.fn((_: String,
                            callback: (err: ExecException | null, stdOut: String, stdError: String) => void) => {
        callback(null, '', '');
      });

      const script = new JavaScript(exec);
      const response = await script.execute();

      expect(exec.mock.calls[0][0]).toEqual(`sh ${JAVA_INIT_COMPILE_RUN} 2>&1`);
      expect(response).toEqual({success: true});
    });
  });
});
