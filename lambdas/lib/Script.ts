import {ExecFunction} from './types';
import JavaErrorParser from './java/JavaErrorParser';
import CsharpErrorParser from './csharp/CsharpErrorParser';
import {ExecException} from 'child_process';
import Response from './Response/Response';
import {CSHARP_INIT_COMPILE_RUN, JAVA_INIT_COMPILE_RUN} from './ScriptLocations';

export abstract class Script<$SuccessResultType, $ErrorResultType> {
  protected constructor(private exec: ExecFunction) {
  }

  protected abstract successResult(stdOut: string): $SuccessResultType;
  protected abstract errorResult(_stdOut: string, error: ExecException, _stdError?: string): $ErrorResultType;
  abstract execute(command?: string): Promise<$SuccessResultType>;

  protected _execute(command: string): Promise<$SuccessResultType> {
    return new Promise<$SuccessResultType>((resolve, reject) => {
      this.exec(command, (err: ExecException | null, stdOut: string, stdError: string) => {
        console.log('stdError', stdError);
        console.log('stdOut', stdOut);
        console.log('error', err);
        if (err) {
          return reject(this.errorResult(stdOut, err, stdError));
        }
        return resolve(this.successResult(stdOut));
      });
    });
  }
}

export class ScriptWithNoOutput extends Script<void, ExecException> {

  constructor(exec: ExecFunction) {
    super(exec);
  }

  protected successResult(_stdOut: string): void {
    return undefined;
  }

  protected errorResult(_stdOut: string, error: ExecException, _stdError?: string): ExecException {
    return error;
  }

  execute(command: string): Promise<void> {
    return super._execute(command);
  }
}


export class JavaScript extends Script<Response, Response> {

  constructor(exec: ExecFunction) {
    super(exec);
  }

  protected successResult(_stdOut: string): Response {
    return {success: true};
  }

  protected errorResult(stdOut: string, _error: ExecException, _stdError: string): Response {
    return JavaErrorParser.parse(stdOut);
  }

  execute(): Promise<Response> {
    return super._execute(`sh ${JAVA_INIT_COMPILE_RUN} 2>&1`);
  }
}

export class CSharpScript extends Script<Response, Response> {

    constructor(exec: ExecFunction) {
        super(exec);
    }

    protected successResult(_stdOut: string): Response {
        return {success: true};
    }

    protected errorResult(stdOut: string, _error: ExecException, _stdError: string): Response {
        return CsharpErrorParser.parse(stdOut);
    }

    execute(): Promise<Response> {
        return super._execute(`sh ${CSHARP_INIT_COMPILE_RUN} 2>&1`);
    }
}
