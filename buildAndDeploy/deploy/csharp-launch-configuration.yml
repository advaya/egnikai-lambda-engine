AWSTemplateFormatVersion: "2010-09-09"
Description: Creates an AutoScaling Group which uses a LaunchConfiguration to run C# code. This assumes a default VPC present with public subnets

Parameters:
  CommitHashOfDeployable:
    Type: String
    Description: The commit hash of the deployable
  Environment:
    Type: String
    Description: The environment code
    AllowedValues: 
      - dev
      - qa
      - prod
  SecurityKeyName:
    Type: AWS::EC2::KeyPair::KeyName
    Description: Name of an existing EC2 KeyPair to enable SSH access to the ECS instances
  AvgCPUThreshold:
    Type: String
    Description: The average CPU threshold when Auto Scaling should be triggered
  InstanceType:
    Description: EC2 instance type
    Type: String
    Default: t2.micro
    AllowedValues: [t2.micro, t2.small, t2.medium, t2.large, m3.medium, m3.large,
      m3.xlarge, m3.2xlarge, m4.large, m4.xlarge, m4.2xlarge, m4.4xlarge, m4.10xlarge,
      c4.large, c4.xlarge, c4.2xlarge, c4.4xlarge, c4.8xlarge, c3.large, c3.xlarge,
      c3.2xlarge, c3.4xlarge, c3.8xlarge, r3.large, r3.xlarge, r3.2xlarge, r3.4xlarge,
      r3.8xlarge, i2.xlarge, i2.2xlarge, i2.4xlarge, i2.8xlarge]
    ConstraintDescription: Please choose a valid instance type
  minSize:
    Type: String
    Description: The minimum number of EC2 instances for the AutoScaling group
    Default: 1
  maxSize:
    Type: String
    Description: The maximum number of EC2 instances for the AutoScaling group
    Default: 5
  accountId:
    Type: String
    Description: The Account Id with which the stack is created
    Default: 654872652106
  region:
    Type: String
    Description: The region where the stack is created
    Default: us-east-1
  ContainerTimeOut:
    Type: String
    Description: The time limit for CSharp code execution
    Default: 30000

Mappings:
  AWSRegionToAMI:
    us-east-1:
      AMI: ami-97785bed
    us-east-2:
      AMI: ami-f63b1193
    us-west-2:
      AMI: ami-f2d3638a
    us-west-1:
      AMI: ami-824c4ee2
    ap-southeast-1:
      AMI: ami-68097514
    ap-southeast-2:
      AMI: ami-942dd1f6
    ap-south-1:
      AMI: ami-531a4c3c
    cn-north-1:
      AMI: ami-cb19c4a6

Resources:
  AutoScalingGroup:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      AutoScalingGroupName: !Join [ "-", [ "CSharpAutoScalingGroup", Ref: Environment ] ]
      LaunchConfigurationName: !Ref 'CSharpLaunchConfiguration'
      AvailabilityZones: 
        Fn::GetAZs: 
          Ref: 'AWS::Region'
      MinSize: !Ref 'minSize'
      MaxSize: !Ref 'maxSize'
      LoadBalancerNames: 
        - !Join [ "-", [ "CSharpLoadBalancer", Ref: Environment ] ]
      HealthCheckType: EC2
      HealthCheckGracePeriod: 300
    DependsOn: [ CSharpLaunchConfiguration, CSharpLoadBalancer ]
        
  CSharpLaunchConfiguration:
    Type: AWS::AutoScaling::LaunchConfiguration
    Properties:
      IamInstanceProfile: !Ref 'Ec2InstanceProfile'
      ImageId: !FindInMap [AWSRegionToAMI, !Ref 'AWS::Region', AMI]
      SecurityGroups: [!Ref 'CsharpEc2InstanceSecurityGroup']
      InstanceType: !Ref 'InstanceType'
      KeyName: !Ref 'SecurityKeyName'
      UserData:
        Fn::Base64: 
          !Sub |
            #!/bin/bash -ex
            yum update -y
            yum install -y docker
            usermod -a -G docker ec2-user
            service docker start
            chkconfig docker on
            su - ec2-user
            cd /home/ec2-user
            touch /home/ec2-user/.bashrc
            echo export HOME=/home/ec2-user >> /home/ec2-user/.bashrc
            source /home/ec2-user/.bashrc
            $(aws ecr get-login --no-include-email --region us-east-1)
            imageName=${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/egnikai-language-runner:${Environment}-${CommitHashOfDeployable}
            docker pull ${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/egnikai-language-runner:${Environment}-${CommitHashOfDeployable}
            curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh >> install.sh
            . install.sh
            . /home/ec2-user/.nvm/nvm.sh
            source /home/ec2-user/.bashrc
            nvm install 8.11.3
            mkdir egnikai && cd egnikai
            aws s3 cp s3://egnikai-execution-engine/${CommitHashOfDeployable}.tar.tgz ${CommitHashOfDeployable}.tar.tgz && tar zxf ${CommitHashOfDeployable}.tar.tgz
            npm install
            IMAGE_NAME=$imageName TIME_OUT=${ContainerTimeOut} node app.js
    DependsOn: [ CsharpEc2InstanceSecurityGroup ]

  CSharpScalingPolicy:
    Type: 'AWS::AutoScaling::ScalingPolicy'
    Properties:
      AutoScalingGroupName: !Ref AutoScalingGroup
      EstimatedInstanceWarmup: 5
      PolicyType: TargetTrackingScaling
      TargetTrackingConfiguration:
        PredefinedMetricSpecification:
          PredefinedMetricType: ASGAverageCPUUtilization
        TargetValue: !Ref 'AvgCPUThreshold'
    DependsOn: [ AutoScalingGroup ]

  Ec2InstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: /
      Roles: [!Ref 'Ec2Role']

  Ec2Role:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Effect: Allow
          Principal:
            Service: [ec2.amazonaws.com]
          Action: ['sts:AssumeRole']
      Path: /

  Ec2Policy:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: !Join [ "-", [ "Ec2Policy", Ref: Environment ] ]
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Sid: Ec2S3Policy
            Effect: Allow
            Action:
              - s3:GetObject
              - s3:ListBucket
              - s3:PutObject
            Resource: "*"
          - Sid: Ec2ECRPolicy
            Effect: Allow
            Action:
              - ecr:GetAuthorizationToken
              - ecr:BatchCheckLayerAvailability
              - ecr:GetDownloadUrlForLayer
              - ecr:GetRepositoryPolicy
              - ecr:DescribeRepositories
              - ecr:ListImages
              - ecr:DescribeImages
              - ecr:BatchGetImage
            Resource: "*"
      Roles:
        - !Ref Ec2Role

  CSharpLoadBalancerSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupName: !Join [ "-", [ "Egnikai-CSharp-LoadBalancer-SecurityGroup", Ref: Environment ] ]
      GroupDescription: Allows ouside world to talk to C# runner through LoadBalancer
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 80
          ToPort: 80
          CidrIp: 182.74.238.30/32
        - IpProtocol: tcp
          FromPort: 80
          ToPort: 80
          CidrIp: 14.142.51.155/32
    
  CSharpLambdaSourceSecurityGroupIngress:
    Type: "AWS::EC2::SecurityGroupIngress"
    Properties:
      GroupId:
        Fn::GetAtt: [ CSharpLoadBalancerSecurityGroup, GroupId ]
      IpProtocol: tcp
      FromPort: 80
      ToPort: 80
      SourceSecurityGroupId:
        Fn::GetAtt: [ CSharpLoadBalancerSecurityGroup, GroupId ]
    DependsOn: [ CSharpLoadBalancerSecurityGroup ]

  CsharpEc2InstanceSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupName: !Join [ "-", [ "Egnikai-CSharp-Runner-SecurityGroup", Ref: Environment ] ]
      GroupDescription: Allows SSH to the C# code runner box from the world
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 22
          ToPort: 22
          CidrIp: 182.74.238.30/32
        - IpProtocol: tcp
          FromPort: 22
          ToPort: 22

  CSharpEc2InstanceSourceSecurityGroupIngress:
    Type: "AWS::EC2::SecurityGroupIngress"
    Properties:
      GroupId:
        Fn::GetAtt: [ CsharpEc2InstanceSecurityGroup, GroupId ]
      IpProtocol: tcp
      FromPort: 3000
      ToPort: 3000
      SourceSecurityGroupId:
        Fn::GetAtt: [ CsharpEc2InstanceSecurityGroup, GroupId ]
    DependsOn: [ CsharpEc2InstanceSecurityGroup ]

  CSharpLoadBalancer:
    Type: AWS::ElasticLoadBalancing::LoadBalancer
    Properties:
      LoadBalancerName: !Join [ "-", [ "CSharpLoadBalancer", Ref: Environment ] ]
      AccessLoggingPolicy:
        EmitInterval: 5
        Enabled: true
        S3BucketName:
          Fn::ImportValue:
           !Join [ "-", [ "LoadBalancerLogsS3Bucket", Ref: Environment ] ]
        S3BucketPrefix: Egnikai
      ConnectionDrainingPolicy:
        Enabled: true
        Timeout: 60
      # AvailabilityZones:
      #   Fn::GetAZs:
      #     Ref: 'AWS::Region'
      Subnets:
        - subnet-0d473ef4e80f88086
        - subnet-0fa5d97d40dfee47a
        - subnet-08fd525faa9791b40
        - subnet-0e039c653047b9670
        - subnet-0501908530493b8b9
        - subnet-00334dc229f994b6c
      Scheme: internal
      SecurityGroups:
        - Fn::GetAtt: [ CsharpEc2InstanceSecurityGroup, GroupId ]
        - Fn::GetAtt: [ CSharpLoadBalancerSecurityGroup, GroupId ]
      Listeners:
        - LoadBalancerPort: 80
          InstancePort: 3000
          Protocol: HTTP
      HealthCheck:
        HealthyThreshold: 2
        Interval: 10
        Target: HTTP:3000/test/
        Timeout: 5
        UnhealthyThreshold: 5

Outputs:
  CSharpLoadBalancerSecurityGroupId:
    Description: SecurityGroupId to allow lambda to talk to load balancer
    Value:
      Fn::GetAtt: [ CSharpLoadBalancerSecurityGroup, GroupId ]
    Export:
      Name: !Join [ "-", [ "CSharpLoadBalancerSecurityGroupId", Ref: Environment ] ]