import 'source-map-support/register';
import {createCsharpService} from './di';
import CandidateCodeS3 from '../lib/CandidateCodeS3';
import AWS = require('aws-sdk');
import * as fs from 'fs';

async function Execute(): Promise<void> {
  const bucket = process.env.BUCKET_NAME;
  const key = process.env.KEY;
  if(!bucket || !key){
    console.log("Bucket name and Key are not set");
    return;
  }
  const s3 = new AWS.S3();
  const candidateCode = new CandidateCodeS3(bucket, key, s3, fs.createWriteStream)

  console.log(`running compile and run for ${bucket}/${key}`);
  await createCsharpService(candidateCode).execute();
  console.log(`completed running compile and run for ${bucket}/${key}`);
}

Execute();
