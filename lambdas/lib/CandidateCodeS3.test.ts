import {Readable} from 'stream';
import * as AWS from 'aws-sdk';
import CandidateCodeS3 from './CandidateCodeS3';
import {PathLike, WriteStream} from 'fs';

describe.skip('CandidateCodeS3Tests',  () => {
  let readableStream: Readable;
  let createWriteStream: ((path: PathLike) => WriteStream);
  const s3 = new AWS.S3();
  let candidateCodeS3: CandidateCodeS3;
  beforeEach(() => {
    readableStream = new Readable({
      objectMode: true,
      read: function() {
        return this.push({});
      }
    });
    readableStream.emit('data', {});
    s3.getObject = jest.fn().mockReturnValue({
      createReadStream: () => readableStream,
    });
    createWriteStream = jest.fn();
    candidateCodeS3 = new CandidateCodeS3('Bucket', 'Key', s3, createWriteStream);
  });

  it('should download the candidate file from s3', (done) => {
    candidateCodeS3.download()
      .then(() => {
        // @ts-ignore
        expect(s3.getObject.mock.calls[0][0]).toEqual({Bucket: 'Bucket', Key: 'Key'});
        done();
      });
    console.log(readableStream);
    // readableStream.emit('end');
    // expect(candidateCodeS3.download()).resolves.toEqual(undefined);
  });
});