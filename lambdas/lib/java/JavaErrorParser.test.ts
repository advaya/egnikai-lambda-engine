import {compilationFailureConsoleOutput, testFailedConsoleOuput, ouputWithWarnings, onlyHiddenTestFailure} from './testHelpers/TestConstants';
import JavaErrorParser from './JavaErrorParser';
import {ErrorType} from '../Response/Response';

describe('JavaGradleErrorParserTests', () => {

  it('should return the response with error type as compilation failed', () => {
    const response = JavaErrorParser.parse(compilationFailureConsoleOutput);
    expect(response).toEqual({success: false, errorType: ErrorType.COMPILATION_FAILED, message: compilationFailureConsoleOutput});
  });

  it('should return the response with error type as tests failed', () => {
    const response = JavaErrorParser.parse(testFailedConsoleOuput);
    const testsFailureMessage = `There were test failures.\nshouldDetectFiveAsPrime\nshouldDetectThreeAsPrimeNumber`;
    expect(response).toEqual({success: false, errorType: ErrorType.TESTS_FAILED, message: testsFailureMessage});
  });

  it('should ignore warnings, empty lines and parse output for proper result', () => {
    const response = JavaErrorParser.parse(ouputWithWarnings);
    const testsFailureMessage = `There were test failures.\nshouldBeAbleToRemoveAnElementFromImmutableCollection\nshouldAddNewElementToImmutableCollection`;
    expect(response).toEqual({success: false, errorType: ErrorType.TESTS_FAILED, message: testsFailureMessage});
  });

  it('should display hidden testcase names even when only hidden tests are failing', () => {
    const response = JavaErrorParser.parse(onlyHiddenTestFailure);
    const testsFailureMessage = `There were test failures.\nshouldDetectFiveAsPrime`;
    expect(response).toEqual({success: false, errorType: ErrorType.TESTS_FAILED, message: testsFailureMessage});
  });

});