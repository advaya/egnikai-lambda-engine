import * as fs from 'fs';
import * as path from 'path';

export const testFailedConsoleOuput = fs.readFileSync(path.resolve(__dirname, './TestsErrorOutput.txt'), {encoding: 'utf8'});
export const compilationFailureConsoleOutput = fs.readFileSync(path.resolve(__dirname, './CompilationError.txt'), {encoding: 'utf8'});
export const ouputWithWarnings = fs.readFileSync(path.resolve(__dirname, './outputWithWarnings.txt'), {encoding: 'utf8'});
export const onlyHiddenTestFailure = fs.readFileSync(path.resolve(__dirname, './onlyHiddenTestFailure.txt'), {encoding: 'utf8'});

